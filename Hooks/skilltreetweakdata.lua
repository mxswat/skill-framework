local data = SkillTreeTweakData.init
function SkillTreeTweakData:init(tweak_data)
    data(self, tweak_data)

    self.skills.fake_skill = {
        ["name_id"] = "fake_skill",
        ["desc_id"] = "fake_skill",
        ["icon_xy"] = {0, 0},
        [1] = {
            upgrades = {
                
            },
            cost = self.costs.default
        },
        [2] = {
            upgrades = {
                
            },
            cost = self.costs.pro
        }
    }

end