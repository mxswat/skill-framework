function PlayerManager:select_next_item()
	if not self._equipment.selected_index then
		return
	end
	log("self._equipment.selected_index")
	local new_index =
		self._equipment.selected_index + 1 <= #self._equipment.selections and self._equipment.selected_index + 1 or 1
	local valid = false
	local count = #self._equipment.selections[new_index].amount

	for i = 1, count, 1 do
		if Application:digest_value(self._equipment.selections[new_index].amount[i], false) > 0 then
			valid = true
		end
	end

	if valid then
		self._equipment.selected_index = new_index
	end
end

local function add_hud_item(amount, icon)
	if #amount > 1 then
		managers.hud:add_item_from_string(
			{
				amount_str = make_double_hud_string(amount[1], amount[2]),
				amount = amount,
				icon = icon
			}
		)
	else
		managers.hud:add_item(
			{
				amount = amount[1],
				icon = icon
			}
		)
	end
end

old_add_equipment = PlayerManager._add_equipment
function PlayerManager:_add_equipment(params)
	old_add_equipment(self, params)
	log("old_add_equipment Done")
	if self:has_equipment(params.equipment) then
		log("Something must be done here!")
	end
end
